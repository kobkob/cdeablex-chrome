console.log("Begin CodeableX ...");

var css = "@import 'https://cdn.jsdelivr.net/gh/Bbioon/codeable-darkmode-css/style.css'";
var css1 = "@import 'main-cs.css'";

var head = document.head || document.getElementsByTagName('head')[0];
var style = document.createElement('style');
var style1 = document.createElement('style');

head.appendChild(style);

style.type = 'text/css';
if (style.styleSheet){
  // This is required for IE8 and below.
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

style1.type = 'text/css';
if (style1.styleSheet){
  // This is required for IE8 and below.
  style1.styleSheet.cssText = css1;
} else {
  style1.appendChild(document.createTextNode(css1));
}

window.addEventListener("load", function(){

  setTimeout(function(t){
    // TODO: implement me!!!!
    const project_description = document.getElementsByClassName('project-description');
    const project_details = document.getElementsByClassName('details');
    console.log("The details", project_details);
    // TODO: html -> text
    console.log("Description ", project_description);
    for (i = 0; i < project_description.length; i++){
        let desc = project_description.item(i).innerText;

        let doc = nlp(desc);
        let myVerbs = doc.verbs().json();
        console.log("Verb: ", doc.verbs().toInfinitive().all().text());
        //let theVerbs = JSON.parse(myVerbs);
        for (i = 0; i < myVerbs.length; i++){
            console.log("You said ",myVerbs[i].parts.subject,  myVerbs[i].normal, ". You ", myVerbs[i].conjugations.PastTense, " so. May I help with the ", myVerbs[i].conjugations.FutureTense);
        }
    }
  }, 3000);

});


